import { Component, OnInit } from '@angular/core';
import { World } from './objects/world';
import { Manager } from './objects/manager';
import { Product } from './objects/product';
import { Pallier } from './objects/pallier';
import { RestserviceService } from './services/restservice.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  //Booleans Pop-up
  openPopUnlocks: boolean = false;
  openPopCash: boolean = false;
  openPopAngels: boolean = false;
  openPopManagers: boolean = false;
  openPopInvestors: boolean = false;

  world: World = new World();
  server: string;

  test: number = 0;

  managers: Array<Manager> = [
    { id: 1, image: "./assets/forest-trump.jpg", name: 'Forest Trump', runs: "Orienteur de boussoles", price: 500 },
    { id: 2, image: "./assets/perry-black.jpg", name: 'Perry Black', runs: "Remplisseur de gourdes", price: 3000 },
    { id: 3, image: "./assets/richard-ruthless.jpg", name: 'Richard Ruthless', runs: "Réchauffeur de réchaud", price: 6000 },
    { id: 4, image: "./assets/gordie-palmbay.jpg", name: 'Gordie Palmbay', runs: "Porteur de sacs à dos", price: 10000 },
    { id: 5, image: "./assets/heisenbird.jpg", name: 'W. Heisenbird', runs: "Confectionneur de duvets", price: 15000 },
    { id: 6, image: "./assets/jim-thorton.jpg", name: 'Jim Thorton', runs: "Déployeur de tentes", price: 35000 }
  ]

  money: any = '0.00';
  username: string = 'your name';

  //products = Array<Product>();
  products = [
    { id: 1, name: 'Boussole', image: './assets/boussole.png', quantity: 0, price: 8.70 },
    { id: 2, name: 'Gourde', image: './assets/gourde.png', quantity: 0, price: 13.50 },
    { id: 3, name: 'Réchaud', image: './assets/rechaud.png', quantity: 0, price: 23.50 },
    { id: 4, name: 'Sac à dos', image: './assets/sac-dos.png', quantity: 0, price: 49.99 },
    { id: 5, name: 'Duvet', image: './assets/duvet.png', quantity: 0, price: 79.99 },
    { id: 6, name: 'Tente', image: './assets/tente.png', quantity: 0, price: 225.50 }
  ]

  //Managers
  badgeManagers: number = 1;

  constructor(private service: RestserviceService) {
    this.server = service.getServer();
    service.getWorld().then(
      world => {
        this.world = world;
      });
  }

  ngOnInit(): void {
    this.testSlider()
  }

  testSlider() {
    setInterval(() => {
      if(this.test !== 100) {
        this.test += 1;
      } else {
        this.test = 0
      }
      console.log(this.test);
    }, 1)
  }

}

export class Manager {
    id : number;
    name : string;
    image: string;
    runs : string;
    price : number;
}
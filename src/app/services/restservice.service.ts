import { Injectable } from '@angular/core';
import{ HttpClient} from'@angular/common/http';
import { World } from '../objects/world';
import { Pallier } from '../objects/pallier';
import { Product } from '../objects/product';

@Injectable({
  providedIn: 'root'
})
export class RestserviceService {

  server= "http://localhost:8080/";
  user= "";

  constructor(private http: HttpClient) { }

  privatehandleError(error: any): Promise<any> {
    console.error('An error occurred', error); 
    return Promise.reject(error.message|| error);
  }
  
  getWorld(): Promise<World> {
    return this.http.get(this.server + 'adventureisis/generic/world')
    .toPromise().catch(this.privatehandleError);
  }

  getServer(){
    return this.server;
  }
}
